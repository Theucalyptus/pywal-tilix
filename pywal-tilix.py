#!/usr/bin/python

import yaml
import json
from pathlib import Path
from os import path, remove

#Génération des paths
wal_path = Path.home() / ".cache/wal/colors.yml"
tilix_path = Path.home() / ".config/tilix/schemes/pywal-tilix.json"
if path.isfile(tilix_path):
    remove(tilix_path)

#Chargement des couleurs depuis wal en YAML
wal_yamlFile = open(Path.home() / ".cache/wal/colors.yml", 'r')
wal_yaml = yaml.load(wal_yamlFile, Loader=yaml.BaseLoader)
background_color = wal_yaml["special"]["background"]
foreground_color = wal_yaml["special"]["foreground"]
cursor_color = wal_yaml["special"]["cursor"]

palette = []
for i in range(16):
    palette.append(wal_yaml["colors"]["color" + str(i)])


# Export comme un profile tilix
tilix_jsonFile = open("/usr/share/pywal-tilix/default.json", 'r')
tilix_json = json.load(tilix_jsonFile)
tilix_json["palette"] = palette
tilix_json["background-color"] = background_color
tilix_json["foreground-color"] = foreground_color

with open(tilix_path, 'a') as outfile:
    json.dump(tilix_json, outfile)
